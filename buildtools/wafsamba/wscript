#!/usr/bin/env python

# this is a base set of waf rules that everything else pulls in first

import sys, wafsamba, Configure
import Options, os, preproc
from samba_utils import *
from optparse import SUPPRESS_HELP

# this forces configure to be re-run if any of the configure
# sections of the build scripts change. We have to check
# for this in sys.argv as options have not yet been parsed when
# we need to set this. This is off by default until some issues
# are resolved related to WAFCACHE. It will need a lot of testing
# before it is enabled by default.
if '--enable-auto-reconfigure' in sys.argv:
    Configure.autoconfig = True

def set_options(opt):
    opt.tool_options('compiler_cc')

    opt.tool_options('gnu_dirs')

    gr = opt.option_group('library handling options')

    gr.add_option('--bundled-libraries',
                   help=("comma separated list of bundled libraries. May include !LIBNAME to disable bundling a library. Can be 'NONE' or 'ALL' [auto]"),
                   action="store", dest='BUNDLED_LIBS', default='')

    extension_default = Options.options['BUNDLED_EXTENSION_DEFAULT']
    gr.add_option('--bundled-library-extension',
                   help=("name extension for bundled libraries [%s]" % extension_default),
                   action="store", dest='BUNDLED_EXTENSION', default=extension_default)

    extension_exception = Options.options['BUNDLED_EXTENSION_EXCEPTION']
    gr.add_option('--bundled-extension-exception',
                   help=("comman separated list of libraries to not apply extension to [%s]" % extension_exception),
                   action="store", dest='BUNDLED_EXTENSION_EXCEPTION', default=extension_exception)

    builtin_defauilt = Options.options['BUILTIN_LIBRARIES_DEFAULT']
    gr.add_option('--builtin-libraries',
                   help=("command separated list of libraries to build directly into binaries [%s]" % builtin_defauilt),
                   action="store", dest='BUILTIN_LIBRARIES', default=builtin_defauilt)

    gr.add_option('--minimum-library-version',
                   help=("list of minimum system library versions (LIBNAME1:version,LIBNAME2:version)"),
                   action="store", dest='MINIMUM_LIBRARY_VERSION', default='')

    gr.add_option('--disable-shared',
                   help=("Disable all use of shared libraries"),
                   action="store_true", dest='disable_shared', default=False)
    gr.add_option('--disable-rpath',
                   help=("Disable use of rpath for build binaries"),
                   action="store_true", dest='disable_rpath_build', default=False)
    gr.add_option('--disable-rpath-install',
                   help=("Disable use of rpath for installed binaries"),
                   action="store_true", dest='disable_rpath_install', default=False)
    gr.add_option('--nonshared-binary',
                   help=("Disable use of shared libs for the listed binaries"),
                   action="store", dest='NONSHARED_BINARIES', default='')

    opt.add_option('--with-modulesdir',
                   help=("modules directory [PREFIX/modules]"),
                   action="store", dest='MODULESDIR', default='${PREFIX}/modules')

    gr = opt.option_group('developer options')

    gr.add_option('-C',
                   help='enable configure cacheing',
                   action='store_true', dest='enable_configure_cache')
    gr.add_option('--enable-auto-reconfigure',
                   help='enable automatic reconfigure on build',
                   action='store_true', dest='enable_auto_reconfigure')
    gr.add_option('--enable-developer',
                   help=("Turn on developer warnings and debugging"),
                   action="store_true", dest='developer', default=False)
    gr.add_option('--picky-developer',
                   help=("Treat all warnings as errors (enable -Werror)"),
                   action="store_true", dest='picky_developer', default=False)
    gr.add_option('--fatal-errors',
                   help=("Stop compilation on first error (enable -Wfatal-errors)"),
                   action="store_true", dest='fatal_errors', default=False)
    gr.add_option('--enable-gccdeps',
                   help=("Enable use of gcc -MD dependency module"),
                   action="store_true", dest='enable_gccdeps', default=True)
    gr.add_option('--timestamp-dependencies',
                   help=("use file timestamps instead of content for build dependencies (BROKEN)"),
                   action="store_true", dest='timestamp_dependencies', default=False)
    gr.add_option('--pedantic',
		   help=("Enable even more compiler warnings"),
		   action='store_true', dest='pedantic', default=False)

    gr.add_option('--abi-check',
		   help=("Check ABI signatures for libraries"),
		   action='store_true', dest='ABI_CHECK', default=False)
    gr.add_option('--abi-check-disable',
		   help=("Disable ABI checking (used with --enable-developer)"),
		   action='store_true', dest='ABI_CHECK_DISABLE', default=False)
    gr.add_option('--abi-update',
		   help=("Update ABI signature files for libraries"),
		   action='store_true', dest='ABI_UPDATE', default=False)

    gr = opt.add_option_group('cross compilation options')

    gr.add_option('--cross-compile',
		   help=("configure for cross-compilation"),
		   action='store_true', dest='CROSS_COMPILE', default=False)
    gr.add_option('--cross-execute',
		   help=("command prefix to use for cross-execution in configure"),
		   action='store', dest='CROSS_EXECUTE', default='')
    gr.add_option('--cross-answers',
		   help=("answers to cross-compilation configuration (auto modified)"),
		   action='store', dest='CROSS_ANSWERS', default='')
    gr.add_option('--hostcc',
		   help=("set host compiler when cross compiling"),
		   action='store', dest='HOSTCC', default=False)

    # we use SUPPRESS_HELP for these, as they are ignored, and are there only
    # to allow existing RPM spec files to work
    opt.add_option('--build',
		   help=SUPPRESS_HELP,
		   action='store', dest='AUTOCONF_BUILD', default='')
    opt.add_option('--host',
		   help=SUPPRESS_HELP,
		   action='store', dest='AUTOCONF_HOST', default='')
    opt.add_option('--program-prefix',
		   help=SUPPRESS_HELP,
		   action='store', dest='AUTOCONF_PROGRAM_PREFIX', default='')
    opt.add_option('--disable-dependency-tracking',
		   help=SUPPRESS_HELP,
		   action='store_true', dest='AUTOCONF_DISABLE_DEPENDENCY_TRACKING', default=False)

    gr = opt.option_group('dist options')
    gr.add_option('--sign-release',
                   help='sign the release tarball created by waf dist',
                   action='store_true', dest='SIGN_RELEASE')
    gr.add_option('--tag',
                   help='tag release in git at the same time',
                   type='string', action='store', dest='TAG_RELEASE')


@wafsamba.runonce
def configure(conf):
    conf.env.hlist = []
    conf.env.srcdir = conf.srcdir

    if Options.options.timestamp_dependencies:
        conf.ENABLE_TIMESTAMP_DEPENDENCIES()

    conf.SETUP_CONFIGURE_CACHE(Options.options.enable_configure_cache)

    # load our local waf extensions
    conf.check_tool('gnu_dirs')
    conf.check_tool('wafsamba')

    conf.CHECK_CC_ENV()

    conf.check_tool('compiler_cc')

    # we need git for 'waf dist'
    conf.find_program('git', var='GIT')

    # older gcc versions (< 4.4) does not work with gccdeps, so we have to see if the .d file is generated
    if Options.options.enable_gccdeps:
        from TaskGen import feature, after
        @feature('testd')
        @after('apply_core')
        def check_d(self):
            tsk = self.compiled_tasks[0]
            tsk.outputs.append(tsk.outputs[0].change_ext('.d'))

        import Task
        cc = Task.TaskBase.classes['cc']
        oldmeth = cc.run

        cc.run = Task.compile_fun_noshell('cc', '${CC} ${CCFLAGS} ${CPPFLAGS} ${_CCINCFLAGS} ${_CCDEFFLAGS} ${CC_SRC_F}${SRC} ${CC_TGT_F}${TGT[0].abspath(env)}')[0]
        try:
            try:
                conf.check(features='cc testd', fragment='int main() {return 0;}\n', ccflags=['-MD'], mandatory=True, msg='Check for -MD')
            except:
                pass
            else:
                conf.check_tool('gccdeps', tooldir=conf.srcdir + "/buildtools/wafsamba")
        finally:
            cc.run = oldmeth

    # make the install paths available in environment
    conf.env.LIBDIR = Options.options.LIBDIR or '${PREFIX}/lib'
    conf.env.BINDIR = Options.options.BINDIR or '${PREFIX}/bin'
    conf.env.SBINDIR = Options.options.SBINDIR or '${PREFIX}/sbin'
    conf.env.MODULESDIR = Options.options.MODULESDIR
    conf.env.BUNDLED_LIBS = Options.options.BUNDLED_LIBS.split(',')
    conf.env.BUILTIN_LIBRARIES = Options.options.BUILTIN_LIBRARIES.split(',')
    conf.env.DISABLE_SHARED = Options.options.disable_shared
    conf.env.NONSHARED_BINARIES = Options.options.NONSHARED_BINARIES.split(',')

    conf.env.BUNDLED_EXTENSION = Options.options.BUNDLED_EXTENSION
    conf.env.BUNDLED_EXTENSION_EXCEPTION = Options.options.BUNDLED_EXTENSION_EXCEPTION.split(',')

    conf.env.CROSS_COMPILE = Options.options.CROSS_COMPILE
    conf.env.CROSS_EXECUTE = Options.options.CROSS_EXECUTE
    conf.env.CROSS_ANSWERS = Options.options.CROSS_ANSWERS
    conf.env.HOSTCC        = Options.options.HOSTCC

    conf.env.AUTOCONF_BUILD = Options.options.AUTOCONF_BUILD
    conf.env.AUTOCONF_HOST  = Options.options.AUTOCONF_HOST
    conf.env.AUTOCONF_PROGRAM_PREFIX = Options.options.AUTOCONF_PROGRAM_PREFIX

    if (conf.env.AUTOCONF_HOST and
        conf.env.AUTOCONF_BUILD and
        conf.env.AUTOCONF_BUILD != conf.env.AUTOCONF_HOST):
        Logs.error('ERROR: Mismatch between --build and --host. Please use --cross-compile instead')
        sys.exit(1)
    if conf.env.AUTOCONF_PROGRAM_PREFIX:
        Logs.error('ERROR: --program-prefix not supported')
        sys.exit(1)

    # enable ABI checking for developers
    conf.env.ABI_CHECK = Options.options.ABI_CHECK or Options.options.developer
    if Options.options.ABI_CHECK_DISABLE:
        conf.env.ABI_CHECK = False
    try:
        conf.find_program('gdb', mandatory=True)
    except:
        conf.env.ABI_CHECK = False

    conf.CHECK_COMMAND(['uname', '-a'],
                       msg='Checking build system',
                       define='BUILD_SYSTEM',
                       on_target=False)
    conf.CHECK_UNAME()

    # see if we can compile and run a simple C program
    conf.CHECK_CODE('printf("hello world")',
                    define='HAVE_SIMPLE_C_PROG',
                    mandatory=True,
                    execute=True,
                    headers='stdio.h',
                    msg='Checking simple C program')

    # see if we can build shared libs
    if not conf.CHECK_LIBRARY_SUPPORT():
        conf.env.DISABLE_SHARED = True

    # check for rpath
    if not conf.env.DISABLE_SHARED and conf.CHECK_LIBRARY_SUPPORT(rpath=True):
        conf.env.RPATH_ON_BUILD   = not Options.options.disable_rpath_build
        conf.env.RPATH_ON_INSTALL = (conf.env.RPATH_ON_BUILD and
                                     not Options.options.disable_rpath_install)
    else:
        conf.env.RPATH_ON_INSTALL = False
        conf.env.RPATH_ON_BUILD   = False

    # we should use the PIC options in waf instead
    conf.ADD_CFLAGS('-fPIC', testflags=True)

    conf.CHECK_INLINE()

    # check for pkgconfig
    conf.check_cfg(atleast_pkgconfig_version='0.0.0')

    conf.DEFINE('_GNU_SOURCE', 1, add_to_cflags=True)
    conf.DEFINE('_XOPEN_SOURCE_EXTENDED', 1, add_to_cflags=True)

    # get the base headers we'll use for the rest of the tests
    conf.CHECK_HEADERS('stdio.h sys/types.h sys/stat.h stdlib.h stddef.h memory.h string.h',
                       add_headers=True)
    conf.CHECK_HEADERS('strings.h inttypes.h stdint.h unistd.h minix/config.h', add_headers=True)
    conf.CHECK_HEADERS('ctype.h standards.h stdbool.h stdint.h stdarg.h vararg.h', add_headers=True)
    conf.CHECK_HEADERS('limits.h assert.h')

    # see if we need special largefile flags
    conf.CHECK_LARGEFILE()

    if 'HAVE_STDDEF_H' in conf.env and 'HAVE_STDLIB_H' in conf.env:
        conf.DEFINE('STDC_HEADERS', 1)

    conf.CHECK_HEADERS('sys/time.h time.h', together=True)

    if 'HAVE_SYS_TIME_H' in conf.env and 'HAVE_TIME_H' in conf.env:
        conf.DEFINE('TIME_WITH_SYS_TIME', 1)

    conf.define('SHLIBEXT', "so", quote=True)

    conf.CHECK_CODE('long one = 1; return ((char *)(&one))[0]',
                    execute=True,
                    define='WORDS_BIGENDIAN')

    # check if signal() takes a void function
    if conf.CHECK_CODE('return *(signal (0, 0)) (0) == 1',
                       define='RETSIGTYPE_INT',
                       execute=False,
                       headers='signal.h',
                       msg='Checking if signal handlers return int'):
        conf.DEFINE('RETSIGTYPE', 'int')
    else:
        conf.DEFINE('RETSIGTYPE', 'void')

    conf.CHECK_VARIABLE('__FUNCTION__', define='HAVE_FUNCTION_MACRO')

    conf.CHECK_CODE('va_list ap1,ap2; va_copy(ap1,ap2)',
                    define="HAVE_VA_COPY",
                    msg="Checking for va_copy")

    conf.CHECK_CODE('''
                    #define eprintf(...) fprintf(stderr, __VA_ARGS__)
                    eprintf("bla", "bar")
                    ''', define='HAVE__VA_ARGS__MACRO')

    conf.SAMBA_BUILD_ENV()


def build(bld):
    # give a more useful message if the source directory has moved
    relpath = os_path_relpath(bld.curdir, bld.srcnode.abspath())
    if relpath.find('../') != -1:
        Logs.error('bld.curdir %s is not a child of %s' % (bld.curdir, bld.srcnode.abspath()))
        raise Utils.WafError('''The top source directory has moved. Please run distclean and reconfigure''')

    bld.CHECK_MAKEFLAGS()
    bld.SETUP_BUILD_GROUPS()
    bld.ENFORCE_GROUP_ORDERING()
    bld.CHECK_PROJECT_RULES()
