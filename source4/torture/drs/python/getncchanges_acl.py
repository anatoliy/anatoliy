#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Unix SMB/CIFS implementation.
# Copyright (C) Anatoliy Atanasov <anatoliy.atanasov@postpath.com> 2010
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Usage:
#  export DC1=dc1_dns_name
#  export DC2=dc2_dns_name
#  export SUBUNITRUN=$samba4srcdir/scripting/bin/subunitrun
#  PYTHONPATH="$PYTHONPATH:$samba4srcdir/torture/drs/python" $SUBUNITRUN getnchcanges_acl -U"$DOMAIN/$DC_USERNAME"%"$DC_PASSWORD"
#

import sys
import time
import os

sys.path.append("bin/python")
import ldb, samba, sys
from samba.auth import system_session
from ldb import SCOPE_BASE, SCOPE_SUBTREE
from samba.samdb import SamDB

import samba.tests

class account_ctx:
    '''hold context variables'''
    pass

class DrsGetNCChangesACLTestCase(samba.tests.TestCase):

    # RootDSE msg for DC1
    info_dc1 = None
    ldb_dc1 = None
    # RootDSE msg for DC1
    info_dc2 = None
    ldb_dc2 = None

    def setUp(self):
        super(DrsGetNCChangesACLTestCase, self).setUp()

        # connect to DCs singleton
        if self.ldb_dc1 is None:
            DrsGetNCChangesACLTestCase.dc1 = get_env_var("DC1")
            DrsGetNCChangesACLTestCase.ldb_dc1 = connect_samdb(self.dc1)
        if self.ldb_dc2 is None:
            DrsGetNCChangesACLTestCase.dc2 = get_env_var("DC2")
            DrsGetNCChangesACLTestCase.ldb_dc2 = connect_samdb(self.dc2)

        # fetch rootDSEs
        if self.info_dc1 is None:
            ldb = self.ldb_dc1
            res = ldb.search(base="", expression="", scope=SCOPE_BASE, attrs=["*"])
            self.assertEquals(len(res), 1)
            DrsGetNCChangesACLTestCase.info_dc1 = res[0]
        if self.info_dc2 is None:
            ldb = self.ldb_dc2
            res = ldb.search(base="", expression="", scope=SCOPE_BASE, attrs=["*"])
            self.assertEquals(len(res), 1)
            DrsGetNCChangesACLTestCase.info_dc2 = res[0]

        # cache some of RootDSE props
        self.schema_dn = self.info_dc1["schemaNamingContext"][0]
        self.domain_dn = self.info_dc1["defaultNamingContext"][0]
        self.config_dn = self.info_dc1["configurationNamingContext"][0]
        self.forest_level = int(self.info_dc1["forestFunctionality"][0])

        # we will need DCs DNS names for 'net drs' command
        self.dnsname_dc1 = self.info_dc1["dnsHostName"][0]
        self.dnsname_dc2 = self.info_dc2["dnsHostName"][0]
        pass

    def tearDown(self):
        super(DrsGetNCChangesACLTestCase, self).tearDown()

    def _GUID_string(self, guid):
        return self.ldb_dc1.schema_format_value("objectGUID", guid)

    def _check_user(self, sam_ldb, user_orig, is_deleted):
        # search the user by guid as it may be deleted
        guid_str = self._GUID_string(user_orig["objectGUID"][0])
        expression = "(objectGUID=%s)" % guid_str
        res = sam_ldb.search(base=self.domain_dn,
                             expression=expression,
                             controls=["show_deleted:1"])
        self.assertEquals(len(res), 1)
        user_cur = res[0]
        # Deleted Object base DN
        dodn = self._deleted_objects_dn(sam_ldb)
        # now check properties of the user
        name_orig = user_orig["cn"][0]
        name_cur  = user_cur["cn"][0]
        if is_deleted:
            self.assertEquals(user_cur["isDeleted"][0],"TRUE")
            self.assertTrue(not("objectCategory" in user_cur))
            self.assertTrue(not("sAMAccountType" in user_cur))
            self.assertTrue(dodn in str(user_cur["dn"]),
                            "User %s is deleted but it is not located under %s!" % (name_orig, dodn))
            self.assertEquals(name_cur, name_orig + "\nDEL:" + guid_str)
        else:
            self.assertTrue(not("isDeleted" in user_cur))
            self.assertEquals(name_cur, name_orig)
            self.assertEquals(user_orig["dn"], user_cur["dn"])
            self.assertTrue(dodn not in str(user_cur["dn"]))
        pass


    def _create_rwdc_account(self, ctx):
        pass

    def _create_rodc_account(self, ctx):
        pass
        print "Adding %s" % ctx.account_dn
        rec = {
            "dn" : ctx.account_dn,
            "objectClass": "computer",
            "displayname": ctx.samname,
            "samaccountname" : ctx.samname,
            "useraccountcontrol" : str(samba.dsdb.UF_WORKSTATION_TRUST_ACCOUNT |
                                       samba.dsdb.UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION |
                                       samba.dsdb.UF_PARTIAL_SECRETS_ACCOUNT),
            "managedby" : ctx.admin_dn,
            "dnshostname" : ctx.dnshostname,
            "msDS-NeverRevealGroup" : ctx.never_reveal_sid,
            "msDS-RevealOnDemandGroup" : ctx.reveal_sid}
        ctx.samdb.add(rec)

        print "Adding %s" % ctx.server_dn
        rec = {
            "dn": ctx.server_dn,
            "objectclass" : "server",
            "systemFlags" : str(samba.dsdb.SYSTEM_FLAG_CONFIG_ALLOW_RENAME |
                                samba.dsdb.SYSTEM_FLAG_CONFIG_ALLOW_LIMITED_MOVE |
                                samba.dsdb.SYSTEM_FLAG_DISALLOW_MOVE_ON_DELETE),
            "serverReference" : ctx.acct_dn,
            "dnsHostName" : ctx.dnshostname}
        ctx.samdb.add(rec)

        print "Adding %s" % ctx.ntds_dn
        rec = {
            "dn" : ctx.ntds_dn,
            "objectclass" : "nTDSDSA",
            "objectCategory" : "CN=NTDS-DSA-RO,%s" % ctx.schema_dn,
            "systemFlags" : str(samba.dsdb.SYSTEM_FLAG_DISALLOW_MOVE_ON_DELETE),
            "dMDLocation" : ctx.schema_dn,
            "options" : "37",
            "msDS-Behavior-Version" : "4",
            "msDS-HasDomainNCs" : ctx.base_dn,
            "msDS-HasFullReplicaNCs" : [ ctx.base_dn, ctx.config_dn, ctx.schema_dn ]}
        ctx.samdb.add(rec, ["rodc_join:1:1"])

        # find the GUID of our NTDS DN
        res = ctx.samdb.search(base=ctx.ntds_dn, scope=ldb.SCOPE_BASE, attrs=["objectGUID"])
        ctx.ntds_guid = misc.GUID(ctx.samdb.schema_format_value("objectGUID", res[0]["objectGUID"][0]))

        print "Adding %s" % ctx.connection_dn
        rec = {
            "dn" : ctx.connection_dn,
            "objectclass" : "nTDSConnection",
            "enabledconnection" : "TRUE",
            "options" : "65",
            "fromServer" : ctx.dc_ntds_dn}
        ctx.samdb.add(rec)

        print "Adding %s" % ctx.topology_dn
        rec = {
            "dn" : ctx.topology_dn,
            "objectclass" : "msDFSR-Member",
            "msDFSR-ComputerReference" : ctx.acct_dn,
            "serverReference" : ctx.ntds_dn}
        ctx.samdb.add(rec)

        print "Adding HOST SPNs to %s" % ctx.acct_dn
        m = ldb.Message()
        m.dn = ldb.Dn(ctx.samdb, ctx.acct_dn)
        SPNs = [ "HOST/%s" % ctx.myname,
                 "HOST/%s" % ctx.dnshostname ]
        m["servicePrincipalName"] = ldb.MessageElement(SPNs,
                                                       ldb.FLAG_MOD_ADD,
                                                       "servicePrincipalName")
        ctx.samdb.modify(m)

        print "Setting account password for %s" % ctx.samname
        ctx.samdb.setpassword("(&(objectClass=user)(sAMAccountName=%s))" % ctx.samname,
                              ctx.acct_pass,
                              force_change_at_next_login=False,
                              username=ctx.samname)



        pass

    def test_GetNCChangesACL(self):
        ctx = account_ctx()
        ctx.samdb = self.ldb_dc1
        self._create_rwdc_account(ctx)
        self._create_rodc_account(ctx)

        pass



########################################################################################
def get_env_var(var_name):
    if not var_name in os.environ.keys():
        raise AssertionError("Please supply %s in environment" % var_name)
    return os.environ[var_name]

def connect_samdb(samdb_url):
    ldb_options = []
    if not "://" in samdb_url:
        if os.path.isfile(samdb_url):
            samdb_url = "tdb://%s" % samdb_url
        else:
            samdb_url = "ldap://%s" % samdb_url
            # user 'paged_search' module when connecting remotely
            ldb_options = ["modules:paged_searches"]

    return SamDB(url=samdb_url,
                 lp=samba.tests.env_loadparm(),
                 session_info=system_session(),
                 credentials=samba.tests.cmdline_credentials,
                 options=ldb_options)

