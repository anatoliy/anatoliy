/* 
   Unix SMB/CIFS implementation.

   common macros for the dcerpc server interfaces

   Copyright (C) Stefan (metze) Metzmacher 2004
   Copyright (C) Andrew Tridgell 2004
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _DCERPC_SERVER_COMMON_H_
#define _DCERPC_SERVER_COMMON_H_

struct share_config;
struct dcesrv_context;
struct dcesrv_context;
struct dcesrv_call_state;
struct ndr_interface_table;

struct dcerpc_server_info { 
	const char *domain_name;
	uint32_t version_major;
	uint32_t version_minor;
	uint32_t version_build;
};

enum srvsvc_PlatformId;
enum srvsvc_ShareType;

/* formatOffered: The format of the names in DsCrackNames. 
   it may be one of the values from DS_NAME_FORMAT (section 4.1.4.1.3) or one of the following.
*/
#define DRSUAPI_DS_NAME_FORMAT_UPN_AND_ALTSECID ( 0xFFFFFFEF )
#define DRSUAPI_DS_NAME_FORMAT_NT4_ACCOUNT_NAME_SANS_DOMAIN_EX ( 0xFFFFFFF0 )
#define DRSUAPI_DS_NAME_FORMAT_LIST_GLOBAL_CATALOG_SERVERS ( 0xFFFFFFF1 )
#define DRSUAPI_DS_NAME_FORMAT_UPN_FOR_LOGON ( 0xFFFFFFF2 )
#define DRSUAPI_DS_NAME_FORMAT_LIST_SERVERS_WITH_DCS_IN_SITE ( 0xFFFFFFF3 )
#define DRSUAPI_DS_NAME_FORMAT_STRING_SID_NAME ( 0xFFFFFFF4 )
#define DRSUAPI_DS_NAME_FORMAT_ALT_SECURITY_IDENTITIES_NAME ( 0xFFFFFFF5 )
#define DRSUAPI_DS_NAME_FORMAT_LIST_NCS ( 0xFFFFFFF6 )
#define DRSUAPI_DS_NAME_FORMAT_LIST_DOMAINS ( 0xFFFFFFF7 )
#define DRSUAPI_DS_NAME_FORMAT_MAP_SCHEMA_GUID ( 0xFFFFFFF8 )
#define DRSUAPI_DS_NAME_FORMAT_NT4_ACCOUNT_NAME_SANS_DOMAIN ( 0xFFFFFFF9 )
#define DRSUAPI_DS_NAME_FORMAT_LIST_ROLES ( 0xFFFFFFFA )
#define DRSUAPI_DS_NAME_FORMAT_LIST_INFO_FOR_SERVER ( 0xFFFFFFFB )
#define DRSUAPI_DS_NAME_FORMAT_LIST_SERVERS_FOR_DOMAIN_IN_SITE ( 0xFFFFFFFC )
#define DRSUAPI_DS_NAME_FORMAT_LIST_DOMAINS_IN_SITE ( 0xFFFFFFFD )
#define DRSUAPI_DS_NAME_FORMAT_LIST_SERVERS_IN_SITE ( 0xFFFFFFFE )
#define DRSUAPI_DS_NAME_FORMAT_LIST_SITES ( 0xFFFFFFFF )

#include "rpc_server/common/proto.h"

#endif /* _DCERPC_SERVER_COMMON_H_ */
