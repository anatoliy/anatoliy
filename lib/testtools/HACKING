===================================
Notes for contributing to testtools
===================================

Coding style
------------

In general, follow PEP 8 <http://www.python.org/dev/peps/pep-0008/>.

For consistency with the standard library's ``unittest`` module, method names
are generally ``camelCase``.

testtools supports Python 2.4 and later, so avoid any 2.5-only features like
the ``with`` statement.


Copyright assignment
--------------------

Part of testtools raison d'etre is to provide Python with improvements to the
testing code it ships. For that reason we require all contributions (that are
non-trivial) to meet one of the following rules:

 - be inapplicable for inclusion in Python.
 - be able to be included in Python without further contact with the
   contributor.
 - be copyright assigned to Jonathan M. Lange.

Please pick one of these and specify it when contributing code to testtools.


Licensing
---------

All code that is not copyright assigned to Jonathan M. Lange (see Copyright
Assignment above) needs to be licensed under the MIT license that testtools
uses, so that testtools can ship it.


Testing
-------

Please write tests for every feature.  This project ought to be a model
example of well-tested Python code!

Take particular care to make sure the *intent* of each test is clear.

You can run tests with ``make check``, or by running ``./run-tests`` directly.


Source layout
-------------

The top-level directory contains the ``testtools/`` package directory, and
miscellaneous files like README and setup.py.

The ``testtools/`` directory is the Python package itself.  It is separated
into submodules for internal clarity, but all public APIs should be “promoted”
into the top-level package by importing them in ``testtools/__init__.py``.
Users of testtools should never import a submodule, they are just
implementation details.

Tests belong in ``testtools/tests/``.


Commiting to trunk
------------------

Testtools is maintained using bzr, with its trunk at lp:testtools. This gives
every contributor the ability to commit their work to their own branches.
However permission must be granted to allow contributors to commit to the trunk
branch.

Commit access to trunk is obtained by joining the testtools-devs Launchpad
team. Membership in this team is contingent on obeying the testtools
contribution policy, including assigning copyright of all the work one creates
and places in trunk to Jonathan Lange.


Code Review
-----------

All code must be reviewed before landing on trunk. The process is to create a
branch in launchpad, and submit it for merging to lp:testtools. It will then
be reviewed before it can be merged to trunk. It will be reviewed by someone:

 * not the author
 * a committer (member of the testtools-devs team)

As a special exception, while the testtools committers team is small and prone
to blocking, a merge request from a committer that has not been reviewed after
24 hours may be merged by that committer. When the team is larger this policy
will be revisited.

Code reviewers should look for the quality of what is being submitted,
including conformance with this HACKING file.

Changes which all users should be made aware of should be documented in NEWS.


NEWS management
---------------

The file NEWS is structured as a sorted list of releases. Each release can have
a free form description and more or more sections with bullet point items.
Sections in use today are 'Improvements' and 'Changes'. To ease merging between
branches, the bullet points are kept alphabetically sorted. The release NEXT is
permanently present at the top of the list.


Release tasks
-------------

In no particular order:

* Choose a version number.

* Ensure __init__ has that version.

* Add a version number to NEWS immediately below NEXT.

* Possibly write a blurb into NEWS.

* Replace any additional references to NEXT with the version being released.
  (should be none).

* Create a source distribution and upload to pypi ('make release').

* Upload to Launchpad as well.

* If a new series has been created (e.g. 0.10.0), make the series on Launchpad.

* Merge or push the release branch to trunk.

* Make a new milestone for the *next release*. We don't really know how we want
  to handle these yet, so this is a suggestion not actual practice:

  * during release we rename NEXT to $version.

  * we call new milestones NEXT.
